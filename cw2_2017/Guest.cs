﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2_2017
{
    public class Guest
    {
        private string name;
        private int age;
        private string passport;

        public string Name
        {
            get { return name; }
            set {
                if (value == "")
                    throw new Exception("name cannot be empty");
                else
                    name = value;
                }
        }

        public int Age
        {
            get { return age; }
            set
            {
                if (value >= 101 || value <= 0)
                    throw new Exception("Invalid Age");
                else
                    age = value;      
            }
        }

        public string Passport
        {
            get { return passport; }
            set 
            {
                if (value.Length > 10 && value != null)
                    throw new Exception("Passport number Invalid");
                else
                    passport = value;
            }
        }
    }
}
