﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2_2017
{
    public class Extras
    {
        private bool echeck;
        private string erequire;
        private bool bcheck;
        private string brequire;
        private bool carcheck;
        private DateTime csdate;
        private DateTime cedate;
        private string chname;
        private int extracost;

        public bool Echeck
        {
            get { return echeck; }
            set { echeck = value; }
        }

        public string Erequire
        {
            get { return erequire; }
            set
            {
                if (echeck == false)
                {
                    if (value == "")
                        erequire = value;
                    else
                        throw new Exception("Not checked for evening meals");
                }
                else
                {
                    if (value != "")
                        erequire = value;
                    else
                        throw new Exception("Please select dietry requirements(if none state none)");
                }
            }
        }

        public bool Bcheck
        {
            get { return bcheck; }
            set { bcheck = value; }
        }

        public string Brequire
        {
            get { return brequire; }
            set
            {
                if (bcheck == false)
                {
                    if (value == "")
                        brequire = value;
                    else
                        throw new Exception("Not checked for evening meals");
                }
                else
                {
                    if (value != "")
                        brequire = value;
                    else
                        throw new Exception("Please select dietry requirements(if none state none)");
                }
            }
        }

        public bool Carcheck
        {
            get { return carcheck; }
            set { carcheck = value; }
        }

        public DateTime Csdate
        {
            get { return csdate; }
            set { csdate = DateTime.Now; }

        }

        public DateTime Cedate
        {
            get { return cedate; }
            set { cedate = DateTime.Now; }
            
            
        }

        public string Chname
        {
            get { return chname; }
            set
            {
                if (carcheck == false)
                {
                    if (value == "")
                        chname = value;
                    else
                        throw new Exception("Not checked for car hire");
                }
                else
                {
                    if (value != "")
                        chname = value;
                    else
                        throw new Exception("Please enter a driver name");
                }
            }
        }

        public int Extracost
        {
            get { return extracost; }
            set { extracost = value; }
        }
    }
}