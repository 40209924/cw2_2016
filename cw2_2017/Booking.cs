﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2_2017
{
    public class Booking
    {
        private int bookingref;
        private DateTime adate;
        private DateTime ddate;
        private int custref;
        private int bookingcost;
        private int guestcost;
        private int finalcost;
        public List<Guest> Guests = new List<Guest>();
        public Extras extra = new Extras();

        public int Bookingref
        {
            get { return bookingref; }
            set { bookingref = value; }
        }

        public DateTime Adate
        {
            get { return adate; }
            set 
            {
                try
                {
                    if (value >= DateTime.Now)
                        adate = value;
                    else
                        throw new Exception("Please choose valid arrival date");
                }
                catch
                {
                    System.Windows.MessageBox.Show("Please choose valid arrival date");
                }
            }
        }

        public DateTime Ddate
        {
            get { return ddate; }
            set
            {
                if ( value > adate)
                    ddate = value;
                else
                    throw new Exception("Please choose valid departure date");
            }
        }

        public int Custref
        {
            get { return custref; }
            set 
            {
                if (value > 0)
                    custref = value;
                else
                    throw new Exception("Invalid customer reference number");
            }
        }
        public int Bookingcost
        {
            get { return bookingcost; }
            set { bookingcost = value; }
        }

        public int Guestcost
        {
            get { return guestcost; }
            set { guestcost = value; }
        }

        public int Finalcost
        {
            get { return finalcost; } 
            set { finalcost = value; }
        }

        public Extras Extras
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public Guest Guest
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public string BookingString()
        {
            //Used to write booking information to file by making it a single string
            string b = (bookingref.ToString() + ", " + adate.ToString() + ", " + ddate.ToString() + ", " + custref.ToString() + "\n");
            return b;
        }

        public string GuestString()
        {
            //used to write each guest to a file by making them a single string
            string g = "";
            foreach (Guest guest in Guests)
                g += ("booking reference" + bookingref.ToString() + ", " + guest.Name + ", " + guest.Age.ToString() + ", " + guest.Passport + "\n");
            return g;
        }

        public string ExtraString()
        {
            //write extra to file by making it a single string
            string e = ("booking reference " + bookingref.ToString() + ", " + extra.Echeck.ToString() + " " + extra.Erequire + ", " + extra.Bcheck.ToString() + " " + extra.Brequire + ", " + extra.Carcheck.ToString() + ", " + extra.Csdate.ToString() + ", " + extra.Cedate.ToString() + ", " + extra.Chname + "\n");
            return e; 
        }
    }
}
