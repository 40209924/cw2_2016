﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw2_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int nights = 0;
        int extracost = 0;
        int bookingcost = 0;
        int guestcost = 0;
        int finalcost = 0; 
        int bookingref = 0;
        int custref = 0;
        int guests = 0;
        List<Customer> Customers = new List<Customer>();
        List<Booking> Bookings = new List<Booking>();
        List<Guest> guestlist = new List<Guest>();

        private void btn_bsave_Click(object sender, RoutedEventArgs e)
        {
            //uses if amending and not making new instance
            if (txt_amendbook.Text != "")
            {
                foreach (Booking b in Bookings)
                {
                    if (b.Bookingref == Int32.Parse(txt_amendbook.Text))
                    {
                        //sets values for everything in booking class
                        b.Custref = Int32.Parse(txt_custref.Text);
                        b.Adate = Convert.ToDateTime(dte_adate.Text);
                        b.Ddate = Convert.ToDateTime(dte_ddate.Text);
                        nights = Convert.ToInt32((b.Ddate - b.Adate).TotalDays);
                        if (box_emeals.IsChecked == true)
                            b.extra.Echeck = true;
                        else
                            b.extra.Echeck = false;
                        if (box_bmeals.IsChecked == true)
                            b.extra.Bcheck = true;
                        else
                            b.extra.Bcheck = false;
                        if (box_hire.IsChecked == true)
                        {
                            //validation for car hire dates
                            b.extra.Carcheck = true;
                            if (dte_cedate.Text == "" || dte_csdate.Text == "")
                                throw new Exception("Invalid start and/or end date");
                            if (b.Adate <= Convert.ToDateTime(dte_csdate.Text) && b.Ddate > Convert.ToDateTime(dte_csdate.Text))
                                b.extra.Csdate = Convert.ToDateTime(dte_csdate.Text);
                            else
                                throw new Exception("Please choose valid start date");
                            if (b.Ddate >= Convert.ToDateTime(dte_cedate.Text) && b.Adate < Convert.ToDateTime(dte_cedate.Text) && b.extra.Csdate < Convert.ToDateTime(dte_cedate.Text))
                                b.extra.Cedate = Convert.ToDateTime(dte_cedate.Text);
                            else
                                throw new Exception("Please choose valid end date");
                        }
                        else
                            b.extra.Carcheck = false;
                        if (box_hire.IsChecked == false && (dte_csdate.Text != "" | dte_cedate.Text != ""))
                            throw new Exception("car hire is not ticked");
                        b.extra.Chname = txt_chname.Text;
                        b.extra.Brequire = txt_brequire.Text;
                        b.extra.Erequire = txt_emeals.Text;
                        //makes sure list of guests in booking class is empty
                        b.Guests.Clear();
                        //adds all guests in main window guestlist to booking guestlist
                        foreach (Guest g in guestlist)
                        {
                            b.Guests.Add(g);
                        } 
                        //calculates the cost per night
                        foreach (Guest g in b.Guests)
                        {
                            if (g.Age >= 18)
                                guestcost = guestcost + 50;
                            else
                                guestcost = guestcost + 30;
                        }
                        b.Guestcost = guestcost;
                        //calculates the cost excluding extras
                        bookingcost = guestcost * nights;
                        b.Bookingcost = bookingcost;
                        //calculates the cost of extras
                        if (box_emeals.IsChecked == true)
                            extracost = extracost + (15 * nights * b.Guests.Count());
                        if (box_bmeals.IsChecked == true)
                            extracost = extracost + (5 * nights * b.Guests.Count());
                        if (box_hire.IsChecked == true)
                            extracost = extracost + (50 * Convert.ToInt32((b.extra.Cedate - b.extra.Csdate).TotalDays));
                        b.extra.Extracost = extracost;
                        //calculates the cost including extras
                        finalcost = bookingcost + extracost;
                        b.Finalcost = finalcost;
                        //Deletes all files written to
                        System.IO.File.Delete("extra.csv");
                        System.IO.File.Delete("booking.csv");
                        System.IO.File.Delete("guest.csv");
                        //rewrites deleted files with amendments 
                        System.IO.File.AppendAllText("booking.csv", b.BookingString());
                        foreach (Booking book in Bookings)
                            System.IO.File.AppendAllText("guest.csv", book.GuestString());
                        System.IO.File.AppendAllText("extra.csv", b.ExtraString());
                        //clears booking window
                        dte_ddate.SelectedDate = null;
                        txt_custref.Clear();
                        dte_adate.SelectedDate = null;
                        guestlist.Clear();
                        lst_guests.Items.Clear();
                        box_hire.IsChecked = false;
                        box_emeals.IsChecked = false;
                        box_bmeals.IsChecked = false;
                        txt_brequire.Clear();
                        txt_emeals.Clear();
                        txt_chname.Clear();
                        dte_csdate.SelectedDate = null;
                        dte_cedate.SelectedDate = null;
                        extracost = 0;
                        bookingcost = 0;
                        guestcost = 0;
                        finalcost = 0;
                        MessageBox.Show("Booking Amended");
                    }
                }
            }
            else
            {
                //checks a guest has been inputted
                if (guests == 0)
                    MessageBox.Show("Please add guest(s)");
                else
                {
                    try
                    {
                        //Makes sure customer refernce exists
                        if (custref >= Int32.Parse(txt_custref.Text))
                        {
                            //same as with amending except uses new instance of booking
                            Booking booking = new Booking();
                            booking.Custref = Int32.Parse(txt_custref.Text);
                            booking.Adate = Convert.ToDateTime(dte_adate.Text);
                            booking.Ddate = Convert.ToDateTime(dte_ddate.Text);
                            nights = Convert.ToInt32((booking.Ddate - booking.Adate).TotalDays);
                            if (box_emeals.IsChecked == true)
                                booking.extra.Echeck = true;
                            else
                                booking.extra.Echeck = false;
                            if (box_bmeals.IsChecked == true)
                                booking.extra.Bcheck = true;
                            else
                                booking.extra.Bcheck = false;
                            if (box_hire.IsChecked == true)
                            {
                                booking.extra.Carcheck = true;
                                if (dte_cedate.Text == "" || dte_csdate.Text == "")
                                    throw new Exception("Invalid start and/or end date");
                                if (booking.Adate <= Convert.ToDateTime(dte_csdate.Text) && booking.Ddate > Convert.ToDateTime(dte_csdate.Text))
                                    booking.extra.Csdate = Convert.ToDateTime(dte_csdate.Text);
                                else
                                    throw new Exception("Please choose valid start date");
                                if (booking.Ddate >= Convert.ToDateTime(dte_cedate.Text) && booking.Adate < Convert.ToDateTime(dte_cedate.Text) && booking.extra.Csdate < Convert.ToDateTime(dte_cedate.Text))
                                    booking.extra.Cedate = Convert.ToDateTime(dte_cedate.Text);
                                else
                                    throw new Exception("Please choose valid end date");
                            }
                            else
                                booking.extra.Carcheck = false;
                            if (box_hire.IsChecked == false && (dte_csdate.Text != "" || dte_cedate.Text != ""))
                                throw new Exception("car hire is not ticked");
                            booking.extra.Chname = txt_chname.Text;
                            booking.extra.Brequire = txt_brequire.Text;
                            booking.extra.Erequire = txt_emeals.Text;
                            foreach (Guest g in guestlist)
                            {
                                booking.Guests.Add(g);
                            }
                            //applis a booking reference and increments
                            bookingref++;
                            booking.Bookingref = bookingref;
                            foreach(Guest g in booking.Guests)
                            {
                                if (g.Age >= 18)
                                    guestcost = guestcost + 50;
                                else
                                    guestcost = guestcost + 30;
                            }
                            booking.Guestcost = guestcost;
                            bookingcost = guestcost * nights;
                            booking.Bookingcost = bookingcost;
                            if (box_emeals.IsChecked == true)
                                extracost = extracost + (15*nights*booking.Guests.Count());
                            if (box_bmeals.IsChecked == true)
                                extracost = extracost + (5*nights*booking.Guests.Count());
                            if (box_hire.IsChecked == true)
                                extracost = extracost + (50*Convert.ToInt32((booking.extra.Cedate - booking.extra.Csdate).TotalDays));
                            booking.extra.Extracost = extracost;
                            finalcost = bookingcost + extracost;
                            booking.Finalcost = finalcost;
                            MessageBox.Show(finalcost.ToString());
                            MessageBox.Show("booking saved, your booking reference number is " + booking.Bookingref + " use this to amend/delete bookings/nThe cost of your booking is £" +finalcost.ToString());
                            dte_ddate.SelectedDate = null;
                            txt_custref.Clear();
                            dte_adate.SelectedDate = null;
                            guestlist.Clear();
                            lst_guests.Items.Clear();
                            box_hire.IsChecked = false;
                            box_emeals.IsChecked = false;
                            box_bmeals.IsChecked = false;
                            txt_brequire.Clear();
                            txt_emeals.Clear();
                            txt_chname.Clear();
                            dte_csdate.SelectedDate = null;
                            dte_cedate.SelectedDate = null;
                            extracost = 0;
                            bookingcost = 0;
                            guestcost = 0;
                            finalcost = 0;
                            //Adds booking to list of other bookings
                            Bookings.Add(booking);
                            //writes to end of each file
                            System.IO.File.AppendAllText("booking.csv", booking.BookingString());
                            foreach (Booking b in Bookings)
                            {
                                System.IO.File.AppendAllText("guest.csv", b.GuestString());
                            }
                            System.IO.File.AppendAllText("extra.csv", booking.ExtraString());
                        }
                        else
                            MessageBox.Show("Customer doesn't exist");                     
                    }
                    catch (Exception x)
                    {
                        MessageBox.Show(x.Message);
                    }
                }
            }
        }
        private void btn_gsave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //checks there aren't too many guests
                if (guests == 4)
                {
                    MessageBox.Show("Maximum Guests added");
                }
                else
                {
                    //creates new instance of guest and assigns values
                    Guest guest = new Guest();
                    guest.Name = txt_gname.Text;
                    guest.Passport = txt_pport.Text;
                    guest.Age = Int32.Parse(txt_age.Text);
                    guestlist.Add(guest);
                    MessageBox.Show("Guest Saved");
                    //adds guest to guestlist
                    lst_guests.Items.Add(guest.Name + ", " + guest.Age + ", " + guest.Passport);
                    txt_gname.Clear();
                    txt_pport.Clear();
                    txt_age.Clear();
                    guests++;
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
        private void btn_csave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //same logic as amending booking
                if (txt_acust.Text != "")
                {
                    foreach (Customer c in Customers)
                    {
                        if (c.Customerref == Int32.Parse(txt_acust.Text))
                        {
                            //applies values to customer with matching id
                            c.Name = txt_cname.Text;
                            c.Address = txt_address.Text;
                            //deletes customer file
                            System.IO.File.Delete("customer.csv");
                            //rewrites customer file
                            foreach (Customer cust in Customers)
                            System.IO.File.AppendAllText("customer.csv", cust.CustomerString());
                            txt_address.Clear();
                            txt_cname.Clear();
                            txt_acust.Clear();
                        }
                    }
                }
                else
                {
                    //creates new instance of customer and assigns values
                    custref++;
                    Customer custom = new Customer();
                    custom.Name = txt_cname.Text;
                    custom.Address = txt_address.Text;
                    custom.Customerref = custref;
                    MessageBox.Show("Customer saved, reference number is " + custom.Customerref + " use this to delete or amend customers or add a booking");
                    //adds customer to list of customers
                    Customers.Add(custom);
                    txt_address.Clear();
                    txt_cname.Clear();
                    //writes to end of file
                    System.IO.File.AppendAllText("customer.csv", custom.CustomerString());
                }
            }
            catch(Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void btn_delguest_Click(object sender, RoutedEventArgs e)
        {
            //removes guest from list box and, in turn, guestlist
            string selected = lst_guests.SelectedItem.ToString();
            lst_guests.Items.Remove(selected);
            guests--;
            RemoveGuest(selected.Split(',')[0]);
        }

        private void btn_amend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //loads values of customer by finding Id in list of customers
                int cref = Int32.Parse(txt_acust.Text);
                Customer cust = Customers.Find(x => x.Customerref == cref);
                txt_cname.Text = cust.Name;
                txt_address.Text = cust.Address;
            }
            catch
            {
                MessageBox.Show("Customer doesn't exist");
            }
        }

        private void btn_delcust_Click(object sender, RoutedEventArgs e)
        {
            //Makes sure number is given
            if (txt_delcust.Text == "")
                MessageBox.Show("No customer reference number entered");
            else
            {
                try
                {
                    //used later to determine if customer is deleted
                    int count = Customers.Count();
                    //looks for and deletes customer if found
                    int cref = Int32.Parse(txt_delcust.Text);
                    Customer cust = Customers.Find(x => x.Customerref == cref);
                    Customers.Remove(cust);
                    //deletes and rewrites customer with deletion implemented
                    System.IO.File.Delete("customer.csv");
                    foreach (Customer c in Customers)
                    {
                        System.IO.File.AppendAllText("customer.csv", c.CustomerString());
                    }
                    //Used to display if Customer was deleted
                    if (Customers.Count() == count)
                        MessageBox.Show("Customer doesn't exist");
                    else
                        MessageBox.Show("Customer Deleted");
                }
                catch
                {
                    MessageBox.Show("Invalid Customer Reference number");
                }
            }
        }

        private void btn_delbook_Click(object sender, RoutedEventArgs e)
        {
            //exactly the same logic as customer
            int count = Bookings.Count();
            int bref = Int32.Parse(txt_delbook.Text);
            Booking book = Bookings.Find(x => x.Bookingref == bref);
            Bookings.Remove(book);
            System.IO.File.Delete("booking.csv");
            System.IO.File.Delete("guest.csv");
            System.IO.File.Delete("extra.csv");
            foreach (Booking b in Bookings)
            {
                System.IO.File.AppendAllText("booking.csv", b.BookingString());
                foreach(Guest g in book.Guests)
                    System.IO.File.AppendAllText("guest.csv", b.GuestString());
                System.IO.File.AppendAllText("extra.csv", b.ExtraString());
            }
            if (count == Bookings.Count())
                MessageBox.Show("Booking doesn't exist");
            else
                MessageBox.Show("Booking Deleted");
        }
        private void btn_amendbook_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //reloads all valaues in booking if refernce is found
                int bref = Int32.Parse(txt_amendbook.Text);
                Booking book = Bookings.Find(x => x.Bookingref == bref);
                guests = book.Guests.Count();
                dte_adate.Text = book.Adate.ToString();
                dte_ddate.Text = book.Ddate.ToString();
                txt_custref.Text = book.Custref.ToString();
                if (book.extra.Echeck == true)
                    box_emeals.IsChecked = true;
                else
                    box_emeals.IsChecked = false;
                if (book.extra.Bcheck == true)
                    box_bmeals.IsChecked = true;
                else
                    box_bmeals.IsChecked = false;
                if (book.extra.Carcheck == true)
                    box_hire.IsChecked = true;
                else
                    box_hire.IsChecked = false;
                dte_csdate.Text = book.extra.Csdate.ToString();
                dte_cedate.Text = book.extra.Csdate.ToString();
                txt_chname.Text = book.extra.Chname;
                txt_brequire.Text = book.extra.Brequire;
                txt_emeals.Text = book.extra.Erequire;
                foreach (Guest g in book.Guests)
                {
                    guestlist.Add(g);
                }
                foreach (Guest g in guestlist)
                {
                    lst_guests.Items.Add(g.Name + ", " + g.Age.ToString() + ", " + g.Passport);
                }
                guests = guestlist.Count();
            }
            catch
            {
                MessageBox.Show("Booking doesn't exist");
            }
        }

        private void btn_invoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //writes all costs of booking
                int bref = Int32.Parse(txt_invoice.Text);
                Booking b = Bookings.Find(x => x.Bookingref == bref);
                lbl_cost.Content = ("The cost of your extras are " + b.extra.Extracost.ToString() + "\nThe cost of your booking per night is " + b.Guestcost.ToString() + "\nThe cost of your entire holiday including extras is " + b.Finalcost.ToString());
            }
            catch
            {
                MessageBox.Show("Booking doesn't exist");
            }
        }
        public void RemoveGuest(string name)
        {
            //used specifically to look remove a guest from the main window guest list using the listbox
            for (int i = 0; i <= guestlist.Count(); i++)
            {
                //looks for the name in the guest list
                if (guestlist[i].Name == name.Split(',')[0])
                {
                    guestlist.RemoveAt(i);
                }
            }
        }
    }
} 