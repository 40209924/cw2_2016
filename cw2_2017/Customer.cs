﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2_2017
{
    public class Customer
    {
        private int customerref;
        private string name;
        private string address;

        public int Customerref
        {
            get { return customerref; }
            set { customerref = value; }
        }

        public string Name
        {
            get { return name; }
            set {
                if (value != "")
                    name = value;
                else
                    throw new Exception("Name cannot be empty");
                }
        }

        public string Address
        {
            get { return address; }
            set {
                if (value != "")
                    address = value;
                else
                    throw new Exception("Name cannot be empty");
                }
        }

        public Booking Booking
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public string CustomerString()
        {
            string c = (customerref.ToString() + ", " + name +  ", " + address + "\n");
            return c;
        }
    }
}
